#!/usr/bin/env python
import threading
import requests
import random
import time
import sys

# https://gist.github.com/vladignatyev/06860ec2040cb497f0f3
def progress(count, total, status=''):
    bar_len = 60
    filled_len = int(round(bar_len * count / float(total)))

    percents = round(100.0 * count / float(total), 1)
    bar = '=' * filled_len + '-' * (bar_len - filled_len)

    sys.stdout.write('[%s] %s%s Target: %s\r' % (bar, percents, '%', status))
    sys.stdout.flush() 
# This procedurally builds out http/https header chunk requests 
def BuildRange(total_size, numsplits):
    target_ranges = \
        [str('%s-%s' % (int(round(1 + i * total_size / (numsplits * 1.0), 0)),
                        int(round(1 + i * total_size / (numsplits * 1.0) +
                                  total_size / (numsplits * 1.0) - 1, 0)))) for i in range(0, numsplits)]
    # fixes first entry 1 to 0
    target_ranges[0] = '0' + target_ranges[0].lstrip('1')
    return target_ranges

# http regenerative asynchronous stream
# Laszlo Coleman
# http://docs.python-requests.org/en/master/user/advanced/
def Main(url):

    session = requests.session()
    inital_request = session.get(
        url,
        headers={
            'Accept-Encoding': 'identity'},
        stream=True)
    total_bytes = int(inital_request.headers['Content-length'])
    mebibytes = total_bytes // 1048576
    remain = total_bytes % 1048576
    if remain > 0:
        mebibytes = mebibytes + 1

    data = {}
    for i in range(0, mebibytes):
        data[i] = None
    
    # build target screen out of total 1Mb chunks 
    target_ranges = BuildRange(total_bytes, mebibytes)

    def RequestChunk(index, n):
        special = {'Range': 'bytes={}'.format(target_ranges[index])}
        try:

                r = session.get(url, headers=special, stream=True, timeout=(60 * n,10))
                data[index] = ''.join([x for x in r.iter_content(None) if x])
        except requests.exceptions.RequestException as e:
            data[index] = None
            # print(e)

    n = 0
    while(data):
        n += 1
        if all(x for x in data.values() if not x):
            break
        # while loop collects missed chunks 
        for x in range(0, len(target_ranges), 5):
            bottle = [(x + y) for y in range(0,5) if (x + y) < len(target_ranges)]
            jobs = []
            
            for key, trg_idx in enumerate(bottle):
                # only assign unfinished jobs to workers, and track indexs
                progress(trg_idx, mebibytes, status=str(trg_idx))
                if data[trg_idx] is None:
                    #print("RequestChunk %s index %s trg_idx %s" %(target_ranges[trg_idx], key, trg_idx))
                    jobs.append(threading.Thread(
                            target=RequestChunk, args=(
                                trg_idx, n),))

            for j in jobs:
                j.start()
            for j in jobs:
                j.join()

            del jobs[:]
        # Standard Google backoff 
        time.sleep(min(64, (2 ** n)) + (random.randint(0, 1000) / 1000.0))
    # Python dicts are always out of order luckily the master index key is 
    # numeric so the sort function reassembles them correctly
    filename = url.rsplit('/', 1)[-1]
    with open(filename, 'wb') as fh:
        for key, value in sorted(data.iteritems()):
            fh.write(value)
    fh.close()


if __name__ == '__main__':

    if len(sys.argv) == 2:


        Main(str(sys.argv[1]))
        sys.stdout.write(str(sys.argv[1]))
        sys.stdout.write("\n")